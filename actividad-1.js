/**
 * 
 * Juan y Tomas quieren comparar su IMC (indice de masa corporal)
 * 
 * IMC= masa / altura^2 = masa / (altura * altura)
 * 
 * Procedimiento 
 * 1. Almacenar en variables la altura y peso de cada uno
 * 2. Calcular el IMC de cada uno
 * 3. Crear una variable booleana que almacene si Juan tiene IMC mayor que Tomas
 * 4. Imprimir un mensaje en la consola con el resultado de la operacion del paso 3. 
 *    (A modo de ejemplo. "El IMC de Juan es mayor al de Tomas? false")
 */