/*
var anioNacimiento1 = 1987;
var anioNacimiento2 = 1988;
var anioNacimiento3 = 1989;
var anioNacimiento4 = 1990;
var anioNacimiento5 = 1991;

var edad1 = 2020 - anioNacimiento1;
var edad2 = 2020 - anioNacimiento2;
var edad3 = 2020 - anioNacimiento3;
var edad4 = 2020 - anioNacimiento4;
var edad5 = 2020 - anioNacimiento5;
*/

//Definicion de la funcion calcularEdad
function calcularEdad(anioNacimiento) {
    return 2020 - anioNacimiento;
}

function holaMundo() {
    console.log("Hola mundo");
}

var edadDaniel = calcularEdad(1987);//Invocamos a la funcion calcularEdad
var edadPepe = calcularEdad(1950);

console.log("La edad de Daniel es "+edadDaniel);
console.log("La edad de Pepe es "+edadPepe);